package main

import (
	"fmt"
	"net/http"
	pCon "randi_firmansyah/controllers/product"
	uCon "randi_firmansyah/controllers/user"

	"github.com/go-chi/chi"
)

func main() {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		r.Get("/product", pCon.GetSemuaProduct)
		r.Post("/product", pCon.PostProduct)
		r.Put("/product/{id}", pCon.UpdateProduct)
		r.Delete("/product/{id}", pCon.DeleteProduct)
	})

	r.Group(func(r chi.Router) {
		r.Get("/user", uCon.GetSemuaUser)
		r.Post("/user", uCon.PostUser)
		r.Put("/user/{id}", uCon.UpdateUser)
		r.Delete("/user/{id}", uCon.DeleteUser)
	})

	fmt.Println("Running Service")

	if err := http.ListenAndServe(":5000", r); err != nil {
		fmt.Println("Error Starting Service")
	}
	fmt.Println("Starting Services")
}
