package user

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"randi_firmansyah/connections/database"
	r "randi_firmansyah/connections/redis"
	"randi_firmansyah/helper/helper"
	h "randi_firmansyah/helper/response"
	"randi_firmansyah/models/userModel"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

var (
	DB    *gorm.DB
	REDIS *redis.Client
	ctx   = context.Background()
)

func init() {
	DB = database.ConnectToDb()
	REDIS = r.ConnectToRedis()
}

func GetSemuaUser(w http.ResponseWriter, r *http.Request) {
	var listUser []userModel.User

	// check redis
	redisData, err := REDIS.Get(ctx, "list_User_randi").Result()
	if err != nil {
		log.Println("Get data from database...")

		// query select *
		DB.Find(&listUser)

		// nge jadiin json
		dataJson, err := json.Marshal(listUser)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		// ngeset ke redis
		err = REDIS.Set(ctx, "list_User_randi", (dataJson), 0).Err()
		if err != nil {
			log.Println("Gagal Set Ke Redis", err)
		}

		h.Response(w, http.StatusOK, h.MsgGetAll("User"), listUser)
		return
	}

	log.Println("Sukses get data from redis   !! ")

	// unmarshall from redis
	var data []userModel.User
	var errUn = json.Unmarshal([]byte(redisData), &data)
	if errUn != nil {
		fmt.Println(errUn.Error())
		return
	}

	h.Response(w, http.StatusOK, h.MsgGetAll("User"), data)
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	// decode from json
	decoder := json.NewDecoder(r.Body)
	var datarequest userModel.User

	// cek
	err := decoder.Decode(&datarequest)
	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	// hash password
	hash, _ := helper.HashPassword(datarequest.Password)
	datarequest.Password = hash

	// query insert
	DB.Create(&datarequest)

	// refresh
	refreshRedis()

	h.Response(w, http.StatusOK, h.MsgTambah("User"), nil)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	// ambil parameter
	id := chi.URLParam(r, "id")

	var datarequest userModel.User

	if id == "" {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	newId, err := strconv.Atoi(id)
	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	datarequest.Id = newId

	// query delete
	DB.Delete(&datarequest)

	refreshRedis()

	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}
	h.Response(w, http.StatusOK, h.MsgHapus("User"), nil)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	idmhs := chi.URLParam(r, "id")

	decoder := json.NewDecoder(r.Body)
	var datarequest userModel.User
	err := decoder.Decode(&datarequest)
	if err != nil {
		h.Response(w, http.StatusInternalServerError, h.MsgServiceErr(), nil)
		return
	}

	// query update
	DB.Model(userModel.User{}).Where("id = ?", idmhs).Updates(datarequest)

	refreshRedis()

	h.Response(w, http.StatusOK, h.MsgUpdate("User"), nil)
}

func refreshRedis() {
	var listUser []userModel.User
	DB.Find(&listUser)

	dataJson, err := json.Marshal(listUser)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// del redis
	REDIS.Del(ctx, "list_User_randi")
	// set redis
	errR := REDIS.Set(ctx, "list_User_randi", (dataJson), 30*time.Second).Err()
	if errR != nil {
		log.Println("Redis Error", err)
	}
}
