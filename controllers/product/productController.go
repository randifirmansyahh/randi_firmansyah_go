package product

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"randi_firmansyah/connections/database"
	r "randi_firmansyah/connections/redis"
	h "randi_firmansyah/helper/response"
	"randi_firmansyah/models/productModel"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

var (
	DB    *gorm.DB
	REDIS *redis.Client
	ctx   = context.Background()
)

func init() {
	DB = database.ConnectToDb()
	REDIS = r.ConnectToRedis()
}

func GetSemuaProduct(w http.ResponseWriter, r *http.Request) {
	var listProduct []productModel.Product

	// check redis
	redisData, err := REDIS.Get(ctx, "list_product_randi").Result()
	if err != nil {
		log.Println("Get data from database...")

		// query select *
		DB.Find(&listProduct)

		// nge jadiin json
		dataJson, err := json.Marshal(listProduct)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		// ngeset ke redis
		err = REDIS.Set(ctx, "list_product_randi", (dataJson), 0).Err()
		if err != nil {
			log.Println("Gagal Set Ke Redis", err)
		}

		h.Response(w, http.StatusOK, h.MsgGetAll("Product"), listProduct)
		return
	}

	log.Println("Sukses get data from redis   !! ")

	// unmarshall from redis
	var data []productModel.Product
	var errUn = json.Unmarshal([]byte(redisData), &data)
	if errUn != nil {
		fmt.Println(errUn.Error())
		return
	}

	h.Response(w, http.StatusOK, h.MsgGetAll("Product"), data)
}

func PostProduct(w http.ResponseWriter, r *http.Request) {
	// decode from json
	decoder := json.NewDecoder(r.Body)
	var datarequest productModel.Product

	// cek
	err := decoder.Decode(&datarequest)
	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	// query insert
	DB.Create(&datarequest)

	// refresh
	refreshRedis()

	h.Response(w, http.StatusOK, h.MsgTambah("Product"), nil)
}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	// ambil parameter
	id := chi.URLParam(r, "id")

	var datarequest productModel.Product

	if id == "" {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	newId, err := strconv.Atoi(id)
	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}

	datarequest.Id = newId

	// query delete
	DB.Delete(&datarequest)

	refreshRedis()

	if err != nil {
		h.Response(w, http.StatusBadRequest, h.MsgInvalidReq(), nil)
		return
	}
	h.Response(w, http.StatusOK, h.MsgHapus("Product"), nil)
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	idmhs := chi.URLParam(r, "id")

	decoder := json.NewDecoder(r.Body)
	var datarequest productModel.Product
	err := decoder.Decode(&datarequest)
	if err != nil {
		h.Response(w, http.StatusInternalServerError, h.MsgServiceErr(), nil)
		return
	}

	// query update
	DB.Model(productModel.Product{}).Where("id = ?", idmhs).Updates(datarequest)

	refreshRedis()

	h.Response(w, http.StatusOK, h.MsgUpdate("Product"), nil)
}

func refreshRedis() {
	var listProduct []productModel.Product
	DB.Find(&listProduct)

	dataJson, err := json.Marshal(listProduct)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// del redis
	REDIS.Del(ctx, "list_product_randi")
	// set redis
	errR := REDIS.Set(ctx, "list_product_randi", (dataJson), 30*time.Second).Err()
	if errR != nil {
		log.Println("Redis Error", err)
	}
}
